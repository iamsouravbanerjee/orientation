Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows

Branch
A version of the repository that diverges from the main working project. Branches can be a new version of a repository, experimental changes, or personal forks of a repository for users to alter and test changes.


Checkout
The git checkout command is used to switch branches in a repository.


Clone
A clone is a copy of a repository or the action of copying a repository. When cloning a repository into another branch, the new branch becomes a remote-tracking branch that can talk upstream to its origin branch (via pushes, pulls, and fetches).


Fetch
By performing a Git fetch, we are downloading and copying that branch’s files to our workstation. Multiple branches can be fetched at once, and we can rename the branches when running the command to suit our needs.


HEAD
HEAD is a reference variable used to denote the most current commit of the repository in which we are working. When we add a new commit, HEAD will then become that new commit.


Master
The primary branch of all repositories. All committed and accepted changes should be on the master branch. We can work directly from the master branch, or create other branches.


Merge
Taking the changes from one branch and adding them into another (traditionally master) branch. These commits are usually first requested via pull request before being merged by a project maintainer.


Origin
The conventional name for the primary version of a repository. Git also uses origin as a system alias for pushing and fetching data to and from the primary branch. For example, git push origin master, when run on a remote, will push the changes to the master branch of the primary repository database.


Pull/Pull Request
If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request. Pull requests ask the repo maintainers to review the commits made, and then, if acceptable, merge the changes upstream. A pull happens when adding the changes to the master branch.


Push
Updates a remote branch with the commits made to the current branch. We are literally “pushing” our changes onto the remote.


Rebase
When rebasing a git commit, we can split the commit, move it, squash it if unwanted, or effectively combine two branches that have diverged from one another.


Remote
A copy of the original branch. When we clone a branch, that new branch is a remote, or clone. Remotes can talk to the origin branch, as well as other remotes for the repository, to make communication between working branches easier.